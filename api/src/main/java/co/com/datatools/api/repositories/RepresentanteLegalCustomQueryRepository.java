package co.com.datatools.api.repositories;


import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.com.datatools.api.models.RepresentanteLegalCustomModel;

@Repository
public interface RepresentanteLegalCustomQueryRepository extends JpaRepository<RepresentanteLegalCustomModel, Integer>{
	
	@Query(value="SELECT r.tipo_documento as tipo_documento, r.nro_documento as nro_documento, r.nombre_completo as nombre_completo,r.direccion as direccion,r.id_ciudad as id_ciudad,c.nombre_ciudad as nombre_ciudad, r.id_departamento as id_departamento, d.nombre_departamento as nombre_departamento,r.id_pais as id_pais, p.nombre_pais as nombre_pais, r.telefono as telefono FROM `representante_legal` r JOIN ciudades c ON r.id_ciudad = c.id_ciudad JOIN departamentos d ON r.id_departamento = d.id_departamento JOIN paises p ON r.id_pais = p.id_pais;", nativeQuery=true)
    List<RepresentanteLegalCustomModel> listarRepresentantes();
	
	@Query(value="SELECT r.tipo_documento as tipo_documento, r.nro_documento as nro_documento, r.nombre_completo as nombre_completo,r.direccion as direccion,r.id_ciudad as id_ciudad,c.nombre_ciudad as nombre_ciudad, r.id_departamento as id_departamento, d.nombre_departamento as nombre_departamento,r.id_pais as id_pais, p.nombre_pais as nombre_pais, r.telefono as telefono FROM `representante_legal` r JOIN ciudades c ON r.id_ciudad = c.id_ciudad JOIN departamentos d ON r.id_departamento = d.id_departamento JOIN paises p ON r.id_pais = p.id_pais where r.nro_documento = ?;", nativeQuery=true)
	Optional<RepresentanteLegalCustomModel> listarRepresentantesPorId(int id);
}
