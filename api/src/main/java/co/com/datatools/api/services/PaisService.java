package co.com.datatools.api.services;

import java.util.Optional;

import co.com.datatools.api.models.PaisModel;

public interface PaisService {
	
	public Iterable<PaisModel> findAll();
	public Optional<PaisModel> findById(int id);

}
