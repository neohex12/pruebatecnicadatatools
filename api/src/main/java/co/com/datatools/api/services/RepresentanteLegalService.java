package co.com.datatools.api.services;

import java.util.Optional;

import co.com.datatools.api.models.RepresentanteLegalModel;


public interface RepresentanteLegalService {
	
	public Optional<RepresentanteLegalModel> findById(int id);
	public RepresentanteLegalModel save(RepresentanteLegalModel legalModel);
	public void deleteById(int id);

}
