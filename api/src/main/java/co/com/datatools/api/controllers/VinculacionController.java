package co.com.datatools.api.controllers;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.datatools.api.models.RepresentanteLegalCustomModel;
import co.com.datatools.api.models.VinculacionCustomModel;
import co.com.datatools.api.models.VinculacionModel;
import co.com.datatools.api.repositories.VinculacionCustomQueryRepository;
import co.com.datatools.api.services.VinculacionService;

@RestController
@RequestMapping("/api/vinculaciones")
public class VinculacionController {
	
	@Autowired
	private VinculacionService vinculacionService;
	
	@Autowired
	private VinculacionCustomQueryRepository vinculacionCustomQueryRepository;
	
	//crear afiliacion vehiculo empresa conductor
	@PostMapping
	public ResponseEntity<?> create (@RequestBody VinculacionModel vinculacionModel){

		ResponseEntity<VinculacionModel> control = ResponseEntity.status(HttpStatus.CREATED).body(vinculacionService.save(vinculacionModel));

		Map<String, Object> response = new HashMap<>();

		if(control.getStatusCodeValue() != 201) {
			response.put("ok", false);
			response.put("mensaje", "Error al realizar la vinculacion, si el error persiste comunicarse con el administrador");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		response.put("ok", true);
		response.put("mensaje", "la vinculacio fue registrada de forma exitosa.");
		response.put("response", control);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}
	
	
	//eliminar empresa por id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarVinculacion (@PathVariable(value = "id") int vinculacionId){
		Optional<VinculacionModel> oVinculo = vinculacionService.findById(vinculacionId);
		Map<String, Object> response = new HashMap<>();
		if(!oVinculo.isPresent()) {
			response.put("ok", false);
			response.put("mensaje", "No se puede encontrar el vinculo a eliminar");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		vinculacionService.deleteById(vinculacionId);
		response.put("ok", true);
		response.put("mensaje", "Vinculo Eliminado de forma satisfactoria con la id "+vinculacionId);
		response.put("response", null);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

		
	}
	
	//obtener vinculaciones de x empresa por su documento
	@GetMapping("/listarVinculaciones/{id}")
	public ResponseEntity<?> listarVinculacionesPorId(@PathVariable(value = "id") BigInteger vinculacionId){
		List<VinculacionCustomModel> lVinculacion = StreamSupport.stream(vinculacionCustomQueryRepository.listarVinculacionesPorId(vinculacionId).spliterator(),false)
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		if(lVinculacion.size()==0) {
			response.put("ok", false);
			response.put("mensaje", "No existen vinculaciones registradas en la base de datos con el documento "+vinculacionId);
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "Se encontraron "+ lVinculacion.size()+ " vinculaciones en la consulta asociadas al documento  "+vinculacionId);
		response.put("response", lVinculacion);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}

}
