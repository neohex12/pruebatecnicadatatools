package co.com.datatools.api.controllers;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.datatools.api.models.EmpresaCustomModel;
import co.com.datatools.api.models.EmpresaModel;
import co.com.datatools.api.models.VinculacionCustomModel;
import co.com.datatools.api.repositories.EmpresaCustomQueryRepository;
import co.com.datatools.api.services.EmpresaService;

@RestController
@RequestMapping("/api/empresas")
public class EmpresaController {
	
	@Autowired
	private EmpresaService empresaService;

	@Autowired
	private EmpresaCustomQueryRepository empresaCustomQueryRepository;

	//crear empresa
	@PostMapping
	public ResponseEntity<?> create (@RequestBody EmpresaModel empresaModel){

		ResponseEntity<EmpresaModel> control = ResponseEntity.status(HttpStatus.CREATED).body(empresaService.save(empresaModel));

		Map<String, Object> response = new HashMap<>();

		if(control.getStatusCodeValue() != 201) {
			response.put("ok", false);
			response.put("mensaje", "Error al realizar el registro de la empresa, si el error persiste comunicarse con el administrador");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		response.put("ok", true);
		response.put("mensaje", "la empresa fue registrada de forma exitosa.");
		response.put("response", control);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}


	//obtener empresa por id
	@GetMapping("/{id}")
	public ResponseEntity<?> obtenerxId (@PathVariable(value = "id") BigInteger empresaId){
		Optional<EmpresaCustomModel> oEmpresa = empresaCustomQueryRepository.listarEmpresasPorId(empresaId);
		Map<String, Object> response = new HashMap<>();
		if(!oEmpresa.isPresent()) {

			response.put("ok", false);
			response.put("mensaje", "No existe empresa registrada con el nro Documento: " + empresaId);
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "se encontro una coincidencia de la empresa identificada con el nro Documento: " + empresaId);
		response.put("response", oEmpresa);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}


	//obtener todas las empresas registradas
	@GetMapping("/listarEmpresas")
	public ResponseEntity<?> listarEmpresas(){
		List<EmpresaCustomModel> lEmpresa = StreamSupport.stream(empresaCustomQueryRepository.listarEmpresas().spliterator(),false)
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		if(lEmpresa.size()==0) {
			response.put("ok", false);
			response.put("mensaje", "No existen empresas registradas en la base de datos");
			response.put("response",null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "Se encontraron "+ lEmpresa.size()+ " empresas en la consulta");
		response.put("response", lEmpresa);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}


	//actualizar empresa
	@PutMapping("/{id}")
	public ResponseEntity<?> actualizarEmpresa (@RequestBody EmpresaModel empresa, @PathVariable(value = "id") BigInteger empresaId){
		Optional<EmpresaModel> oEmpresa = empresaService.findById(empresaId);
		Map<String, Object> response = new HashMap<>();
		if(!oEmpresa.isPresent()) {
			response.put("ok", false);
			response.put("mensaje", "No es posible encontrar el recurso que desea actualizar");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		oEmpresa.get().setDocumento_representante_legal(empresa.getDocumento_representante_legal());
		oEmpresa.get().setNombre_completo(empresa.getNombre_completo());
		oEmpresa.get().setDireccion(empresa.getDireccion());
		oEmpresa.get().setId_ciudad(empresa.getId_ciudad());
		oEmpresa.get().setId_departamento(empresa.getId_departamento());
		oEmpresa.get().setId_pais(empresa.getId_pais());
		oEmpresa.get().setTelefono(empresa.getTelefono());

		ResponseEntity<EmpresaModel> control = ResponseEntity.status(HttpStatus.CREATED).body(empresaService.save(oEmpresa.get()));

		if(control.getStatusCodeValue() != 201) {
			response.put("ok", false);
			response.put("mensaje", "Error al realizar la actualizacion de la empresa, si el error persiste comunicarse con el administrador");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		response.put("ok", true);
		response.put("mensaje", "se actualizo la empresa con nro documento: "+empresaId);
		response.put("response", control);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);



	}

	//eliminar empresa por id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarEmpresa (@PathVariable(value = "id") BigInteger empresaId){
		Optional<EmpresaCustomModel> oEmpresa = empresaCustomQueryRepository.listarEmpresasPorId(empresaId);
		Map<String, Object> response = new HashMap<>();
		if(!oEmpresa.isPresent()) {
			response.put("ok", false);
			response.put("mensaje", "No se puede encontrar la empresa a eliminar");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		empresaService.deleteById(empresaId);
		response.put("ok", true);
		response.put("mensaje", "Empresa Eliminada de forma satisfactoria con la identificacion "+empresaId);
		response.put("response", null);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}
	
	//obtener tipos de documento por empresa 
	@GetMapping("/listarTiposDocumentosPorId/{id}")
	public ResponseEntity<?> listarTiposDocumentosPorId(@PathVariable(value = "id") BigInteger EmpresaId){
		List<String> lEmpresa= StreamSupport.stream(empresaCustomQueryRepository.listarTipoPorId(EmpresaId).spliterator(),false)
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		if(lEmpresa.size()==0) {
			response.put("ok", false);
			response.put("mensaje", "No existen tipos de documentos registrados en la base de datos con el id "+EmpresaId);
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "Se encontraron "+ lEmpresa.size()+ " tipos de documento en la consulta asociadas al id  "+EmpresaId);
		response.put("response", lEmpresa);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}


}
