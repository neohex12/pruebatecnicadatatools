package co.com.datatools.api.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.com.datatools.api.models.ConductorCustomModel;

@Repository
public interface ConductorCustomQueryRepository extends JpaRepository<ConductorCustomModel, Integer> {
	
	@Query(value="SELECT cdr.tipo_documento as tipo_documento, cdr.nro_documento  as nro_documento ,cdr.nombre_completo as nombre_completo,cdr.direccion as direccion,cdr.id_ciudad as id_ciudad,c.nombre_ciudad as nombre_ciudad,cdr.id_departamento as id_departamento, d.nombre_departamento as nombre_departamento,cdr.id_pais as id_pais, p.nombre_pais as nombre_pais, cdr.telefono as telefono FROM conductores cdr  JOIN ciudades c ON cdr.id_ciudad = c.id_ciudad JOIN departamentos d ON cdr.id_departamento = d.id_departamento JOIN paises p ON cdr.id_pais = p.id_pais;", nativeQuery=true)
	Iterable<ConductorCustomModel> listarConductores();

	@Query(value="SELECT cdr.tipo_documento as tipo_documento, cdr.nro_documento  as nro_documento ,cdr.nombre_completo as nombre_completo,cdr.direccion as direccion,cdr.id_ciudad as id_ciudad,c.nombre_ciudad as nombre_ciudad,cdr.id_departamento as id_departamento, d.nombre_departamento as nombre_departamento,cdr.id_pais as id_pais, p.nombre_pais as nombre_pais, cdr.telefono as telefono FROM conductores cdr  JOIN ciudades c ON cdr.id_ciudad = c.id_ciudad JOIN departamentos d ON cdr.id_departamento = d.id_departamento JOIN paises p ON cdr.id_pais = p.id_pais where cdr.nro_documento = ?;", nativeQuery=true)
	Optional<ConductorCustomModel> listarConductorPorId(int id);

}
