package co.com.datatools.api.models;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vehiculos_afiliacion_empresas")
public class VinculacionCustomModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String Placa;
	private String Tipo_Id_Empresa;
	private BigInteger Numero_Documento_empresa;
	private String Nombre_Empresa;
	private String conductor_asignado;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPlaca() {
		return Placa;
	}
	public void setPlaca(String placa) {
		Placa = placa;
	}
	public String getTipo_Id_Empresa() {
		return Tipo_Id_Empresa;
	}
	public void setTipo_Id_Empresa(String tipo_Id_Empresa) {
		Tipo_Id_Empresa = tipo_Id_Empresa;
	}
	public BigInteger getNumero_Documento_empresa() {
		return Numero_Documento_empresa;
	}
	public void setNumero_Documento_empresa(BigInteger numero_Documento_empresa) {
		Numero_Documento_empresa = numero_Documento_empresa;
	}
	public String getNombre_Empresa() {
		return Nombre_Empresa;
	}
	public void setNombre_Empresa(String nombre_Empresa) {
		Nombre_Empresa = nombre_Empresa;
	}
	public String getConductor_asignado() {
		return conductor_asignado;
	}
	public void setConductor_asignado(String conductor_asignado) {
		this.conductor_asignado = conductor_asignado;
	}
	
	

}
