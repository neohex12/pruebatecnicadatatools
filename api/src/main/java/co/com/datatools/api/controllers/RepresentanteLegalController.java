package co.com.datatools.api.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.datatools.api.models.RepresentanteLegalCustomModel;
import co.com.datatools.api.models.RepresentanteLegalModel;
import co.com.datatools.api.repositories.RepresentanteLegalCustomQueryRepository;
import co.com.datatools.api.services.RepresentanteLegalService;


@RestController
@RequestMapping("/api/representante")
public class RepresentanteLegalController {
	
	@Autowired
	private RepresentanteLegalService legalService;

	@Autowired
	private RepresentanteLegalCustomQueryRepository legalRepository;

	//crear representante
	@PostMapping
	public ResponseEntity<?> create (@RequestBody RepresentanteLegalModel legalModel){

		ResponseEntity<RepresentanteLegalModel> control = ResponseEntity.status(HttpStatus.CREATED).body(legalService.save(legalModel));

		Map<String, Object> response = new HashMap<>();

		if(control.getStatusCodeValue() != 201) {
			response.put("ok", false);
			response.put("mensaje", "Error al realizar el registro de del representante legal, si el error persiste comunicarse con el administrador");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		response.put("ok", true);
		response.put("mensaje", "El representante legal fue registrado de forma exitosa.");
		response.put("response", control);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}


	//obtener representante por id
	@GetMapping("/{id}")
	public ResponseEntity<?> obtenerxId (@PathVariable(value = "id") int representante){
		Optional<RepresentanteLegalCustomModel> oRepresentante = legalRepository.listarRepresentantesPorId(representante);
		Map<String, Object> response = new HashMap<>();
		if(!oRepresentante.isPresent()) {

			response.put("ok", false);
			response.put("mensaje", "No existe el presentante registrado con el nro Documento: " + representante);
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "se encontro una coincidencia del representante legal identificado con el nro Documento: " + representante);
		response.put("response", oRepresentante);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}


	//obtener todas los representantes legales
	@GetMapping("/listarRepresentantes")
	public ResponseEntity<?> listarRepresentantes(){
		List<RepresentanteLegalCustomModel> lRepresentante = StreamSupport.stream(legalRepository.listarRepresentantes().spliterator(),false)
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		if(lRepresentante.size()==0) {
			response.put("ok", false);
			response.put("mensaje", "No existen representantes legales registrados en la base de datos");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "Se encontraron "+ lRepresentante.size()+ " representantes legales en la consulta");
		response.put("response", lRepresentante);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}


	//eliminar empresa por id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarRepresentante (@PathVariable(value = "id") int representante){
		Optional<RepresentanteLegalModel> oRepresentante = legalService.findById(representante);
		Map<String, Object> response = new HashMap<>();
		if(!oRepresentante.isPresent()) {
			response.put("ok", false);
			response.put("mensaje", "No se puede encontrar el representante a eliminar");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		legalService.deleteById(representante);
		response.put("ok", true);
		response.put("mensaje", "Representante Eliminado de forma satisfactoria con la identificacion "+representante);
		response.put("response", null);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}

	//actualizar representante
	@PutMapping("/{id}")
	public ResponseEntity<?> actualizarRepresentante (@RequestBody RepresentanteLegalModel legalModel, @PathVariable(value = "id") int representante){
		Optional<RepresentanteLegalModel> oRepresentante = legalService.findById(representante);
		Map<String, Object> response = new HashMap<>();
		if(!oRepresentante.isPresent()) {
			response.put("ok", false);
			response.put("mensaje", "No es posible encontrar el recurso que desea actualizar");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		oRepresentante.get().setNombre_completo(legalModel.getNombre_completo());
		oRepresentante.get().setDireccion(legalModel.getDireccion());
		oRepresentante.get().setId_ciudad(legalModel.getId_ciudad());
		oRepresentante.get().setId_departamento(legalModel.getId_departamento());
		oRepresentante.get().setId_pais(legalModel.getId_pais());
		oRepresentante.get().setTelefono(legalModel.getTelefono());

		ResponseEntity<RepresentanteLegalModel> control = ResponseEntity.status(HttpStatus.CREATED).body(legalService.save(oRepresentante.get()));

		if(control.getStatusCodeValue() != 201) {
			response.put("ok", false);
			response.put("mensaje", "Error al realizar la actualizacion del representante legal, si el error persiste comunicarse con el administrador");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		response.put("ok", true);
		response.put("mensaje", "se actualizo el representante legal con nro documento: "+representante);
		response.put("response", control);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);



	}

}
