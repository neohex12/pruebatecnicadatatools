package co.com.datatools.api.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.com.datatools.api.models.CiudadModel;

@Repository
public interface CiudadRepository extends JpaRepository<CiudadModel, Integer>{
   
	@Query(value="SELECT id_ciudad,id_departamento,nombre_ciudad FROM `ciudades`", nativeQuery=true)
    List<CiudadModel> listarCiudades();
	
	@Query(value="SELECT id_ciudad,id_departamento,nombre_ciudad FROM `ciudades` WHERE id_departamento  = ?", nativeQuery=true)
	List<CiudadModel> listarCiudadesPorId(int id);

}
