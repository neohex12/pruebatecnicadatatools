package co.com.datatools.api.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.datatools.api.models.PaisModel;
import co.com.datatools.api.services.PaisService;

@RestController
@RequestMapping("/api/paises")
public class PaisController {

	@Autowired
	private PaisService paisService;
	
	//obtener pais por id
	@GetMapping("/{id}")
	public ResponseEntity<?> obtenerxId (@PathVariable(value = "id") int paisId){
		Optional<PaisModel> oPais = paisService.findById(paisId);
		Map<String, Object> response = new HashMap<>();
		if(!oPais.isPresent()) {

			response.put("ok", false);
			response.put("mensaje", "No existe el pais registrado con la id: " + paisId);
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("response", oPais);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}	
	
	//obtener todos los paises 
	@GetMapping("/listarPaises")
	public ResponseEntity<?> listarPaises(){
		List<PaisModel> lPais = StreamSupport.stream(paisService.findAll().spliterator(),false)
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		if(lPais.size()==0) {
			response.put("ok", false);
			response.put("mensaje", "No existen paises registrados en la base de datos");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "Se encontraron "+ lPais.size()+ " paises en la consulta");
		response.put("response", lPais);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}
	
}
