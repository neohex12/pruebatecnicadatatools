package co.com.datatools.api.services;

import java.util.Optional;

import co.com.datatools.api.models.VinculacionModel;

public interface VinculacionService {
	
	public Optional<VinculacionModel> findById(int id);
	public VinculacionModel save(VinculacionModel empresaModel);
	public void deleteById(int id);

}
