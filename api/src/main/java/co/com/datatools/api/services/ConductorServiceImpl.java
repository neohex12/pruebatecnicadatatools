package co.com.datatools.api.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.datatools.api.models.ConductorModel;
import co.com.datatools.api.repositories.ConductorRepository;

@Service
public class ConductorServiceImpl implements ConductorService {

	@Autowired
	private ConductorRepository conductorRepository;
	
	@Override
	public Optional<ConductorModel> findById(int id) {
		return conductorRepository.findById(id);
	}

	@Override
	public ConductorModel save(ConductorModel conductorModel) {
		return conductorRepository.save(conductorModel);
	}

	@Override
	public void deleteById(int id) {
		conductorRepository.deleteById(id);
	}

}
