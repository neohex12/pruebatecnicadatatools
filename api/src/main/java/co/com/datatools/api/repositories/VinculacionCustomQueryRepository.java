package co.com.datatools.api.repositories;

import java.math.BigInteger;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.com.datatools.api.models.VinculacionCustomModel;


@Repository
public interface VinculacionCustomQueryRepository extends JpaRepository<VinculacionCustomModel, BigInteger>{
	
	@Query(value="SELECT vaem.id as id,vaem.placa_vehiculo as Placa, CASE WHEN e.tipo = 1 THEN 'NIT' END AS Tipo_Id_Empresa,e.nro_documento_empresa as Numero_Documento_empresa, e.nombre_completo as Nombre_Empresa, vaem.conductor_asignado as conductor_asignado FROM vehiculos_afiliacion_empresas vaem JOIN empresas e ON vaem.id_empresa = e.nro_documento_empresa JOIN vehiculos v ON vaem.placa_vehiculo = v.placa WHERE e.nro_documento_empresa = ?;", nativeQuery=true)
	List<VinculacionCustomModel> listarVinculacionesPorId(BigInteger id);

}
