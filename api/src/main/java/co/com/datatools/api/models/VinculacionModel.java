package co.com.datatools.api.models;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "vehiculos_afiliacion_empresas")
public class VinculacionModel {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private BigInteger id_empresa;
	private String placa_vehiculo;
	private BigInteger conductor_asignado;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public BigInteger getId_empresa() {
		return id_empresa;
	}
	public void setId_empresa(BigInteger id_empresa) {
		this.id_empresa = id_empresa;
	}
	public String getPlaca_vehiculo() {
		return placa_vehiculo;
	}
	public void setPlaca_vehiculo(String placa_vehiculo) {
		this.placa_vehiculo = placa_vehiculo;
	}
	public BigInteger getConductor_asignado() {
		return conductor_asignado;
	}
	public void setConductor_asignado(BigInteger conductor_asignado) {
		this.conductor_asignado = conductor_asignado;
	}
	

	
	

}
