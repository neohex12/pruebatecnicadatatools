package co.com.datatools.api.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.datatools.api.models.CiudadModel;
import co.com.datatools.api.repositories.CiudadRepository;

@RestController
@RequestMapping("/api/ciudades")
public class CiudadController {
	
	@Autowired
	private CiudadRepository ciudadRepository;
	
	//obtener departamento por id
	@GetMapping("/{id}")
	public ResponseEntity<?> obtenerxId (@PathVariable(value = "id") int departamentoId){
		List<CiudadModel> ICiudad = ciudadRepository.listarCiudadesPorId(departamentoId);
		Map<String, Object> response = new HashMap<>();
		if(ICiudad.size()==0) {
			response.put("ok", false);
			response.put("mensaje", "No existen ciudadades registradas asociados con la id departamento "+departamentoId);
			response.put("response", null );
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "Consulta Realizada con exito con el id departmento "+departamentoId);
		response.put("response", ICiudad);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}	
	
	//obtener todos los departamentos 
	@GetMapping("/listarDepartamentos")
	public ResponseEntity<?> listarDepartamentos(){
		List<CiudadModel> ICiudad = StreamSupport.stream(ciudadRepository.listarCiudades().spliterator(),false)
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		if(ICiudad.size()==0) {
			response.put("ok", false);
			response.put("response", "No existen ciudades registradss en la base de datos");
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "Se encontraron "+ ICiudad.size()+ " ciudades en la consulta");
		response.put("response", ICiudad);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}
}
