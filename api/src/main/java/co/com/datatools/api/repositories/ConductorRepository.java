package co.com.datatools.api.repositories;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.datatools.api.models.ConductorModel;


@Repository
public interface ConductorRepository extends JpaRepository<ConductorModel, Integer>{

}
