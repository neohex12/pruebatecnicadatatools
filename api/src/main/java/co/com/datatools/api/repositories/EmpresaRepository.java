package co.com.datatools.api.repositories;

import java.math.BigInteger;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.datatools.api.models.EmpresaModel;



@Repository
public interface EmpresaRepository extends JpaRepository<EmpresaModel, BigInteger>  {


}
