package co.com.datatools.api.repositories;

import java.math.BigInteger;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.com.datatools.api.models.EmpresaCustomModel;


@Repository
public interface EmpresaCustomQueryRepository extends JpaRepository<EmpresaCustomModel, BigInteger>{
	
	@Query(value="SELECT e.tipo as tipo, e.nro_documento_empresa as nro_documento_empresa,e.documento_representante_legal as documento_representante_legal,r.nombre_completo as nombre_representante, e.nombre_completo as nombre_completo,e.direccion as direccion,e.id_ciudad as id_ciudad,c.nombre_ciudad as nombre_ciudad,e.id_departamento as id_departamento, d.nombre_departamento as nombre_departamento,e.id_pais as id_pais, p.nombre_pais as nombre_pais, e.telefono as telefono FROM empresas e JOIN representante_legal r ON e.documento_representante_legal = r.nro_documento JOIN ciudades c ON e.id_ciudad = c.id_ciudad JOIN departamentos d ON e.id_departamento = d.id_departamento JOIN paises p ON e.id_pais = p.id_pais;", nativeQuery=true)
	Iterable<EmpresaCustomModel> listarEmpresas();
		
	@Query(value="SELECT e.tipo as tipo, e.nro_documento_empresa as nro_documento_empresa,e.documento_representante_legal as documento_representante_legal,r.nombre_completo as nombre_representante, e.nombre_completo as nombre_completo,e.direccion as direccion,e.id_ciudad as id_ciudad,c.nombre_ciudad as nombre_ciudad,e.id_departamento as id_departamento, d.nombre_departamento as nombre_departamento,e.id_pais as id_pais, p.nombre_pais as nombre_pais, e.telefono as telefono FROM empresas e JOIN representante_legal r ON e.documento_representante_legal = r.nro_documento JOIN ciudades c ON e.id_ciudad = c.id_ciudad JOIN departamentos d ON e.id_departamento = d.id_departamento JOIN paises p ON e.id_pais = p.id_pais where e.nro_documento_empresa = ?;", nativeQuery=true)
	Optional<EmpresaCustomModel> listarEmpresasPorId(BigInteger id);

	
	@Query(value="SELECT `tipo` FROM `empresas` WHERE `nro_documento_empresa` = ?;", nativeQuery=true)
	Iterable<String> listarTipoPorId(BigInteger id);
}
