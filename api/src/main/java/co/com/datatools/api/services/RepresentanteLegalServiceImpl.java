package co.com.datatools.api.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.datatools.api.models.RepresentanteLegalModel;
import co.com.datatools.api.repositories.RepresentanteLegalRepository;

@Service
public class RepresentanteLegalServiceImpl implements RepresentanteLegalService {

	@Autowired
	private RepresentanteLegalRepository legalRepository;
	
	@Override
	public Optional<RepresentanteLegalModel> findById(int id) {
		return legalRepository.findById(id);
	}

	@Override
	public RepresentanteLegalModel save(RepresentanteLegalModel legalModel) {
		return legalRepository.save(legalModel);
	}

	@Override
	public void deleteById(int id) {
		legalRepository.deleteById(id);
	}

}
