package co.com.datatools.api.services;

import java.math.BigInteger;
import java.util.Optional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import co.com.datatools.api.models.EmpresaModel;
import co.com.datatools.api.repositories.EmpresaRepository;

@Service
public class EmpresaServiceImpl implements EmpresaService{

	@Autowired
	private EmpresaRepository empresaRepository;


	@Transactional
	public EmpresaModel save(EmpresaModel empresaModel) {
		return empresaRepository.save(empresaModel);
	}

	@Transactional
	public void deleteById(BigInteger id) {
		empresaRepository.deleteById(id);
	}

	@Override
	public Optional<EmpresaModel> findById(BigInteger id) {
		 return empresaRepository.findById(id);
	}

}
