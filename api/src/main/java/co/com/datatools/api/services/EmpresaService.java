package co.com.datatools.api.services;

import co.com.datatools.api.models.EmpresaModel;

import java.math.BigInteger;
import java.util.Optional;

public interface EmpresaService {
	
	public Optional<EmpresaModel> findById(BigInteger id);
	public EmpresaModel save(EmpresaModel empresaModel);
	public void deleteById(BigInteger id);

}
