package co.com.datatools.api.models;

import java.math.BigInteger;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "empresas")
public class EmpresaCustomModel {
	
	private int tipo;
	@Id
	private BigInteger nro_documento_empresa;
	private int documento_representante_legal;
	private String nombre_representante;
	private String nombre_completo;
	private String direccion;
	private int id_ciudad;
	private String nombre_ciudad;
	private int id_departamento;
	private String nombre_departamento;
	private int id_pais;
	private String nombre_pais;
	private String telefono;
	
	
	public int getTipo() {
		return tipo;
	}
	public void setTipo(int tipo) {
		this.tipo = tipo;
	}
	public BigInteger getNro_documento_empresa() {
		return nro_documento_empresa;
	}
	public void setNro_documento_empresa(BigInteger nro_documento_empresa) {
		this.nro_documento_empresa = nro_documento_empresa;
	}
	public int getDocumento_representante_legal() {
		return documento_representante_legal;
	}
	public void setDocumento_representante_legal(int documento_representante_legal) {
		this.documento_representante_legal = documento_representante_legal;
	}
	public String getNombre_representante() {
		return nombre_representante;
	}
	public void setNombre_representante(String nombre_representante) {
		this.nombre_representante = nombre_representante;
	}
	public String getNombre_completo() {
		return nombre_completo;
	}
	public void setNombre_completo(String nombre_completo) {
		this.nombre_completo = nombre_completo;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public int getId_ciudad() {
		return id_ciudad;
	}
	public void setId_ciudad(int id_ciudad) {
		this.id_ciudad = id_ciudad;
	}
	public String getNombre_ciudad() {
		return nombre_ciudad;
	}
	public void setNombre_ciudad(String nombre_ciudad) {
		this.nombre_ciudad = nombre_ciudad;
	}
	public int getId_departamento() {
		return id_departamento;
	}
	public void setId_departamento(int id_departamento) {
		this.id_departamento = id_departamento;
	}
	public String getNombre_departamento() {
		return nombre_departamento;
	}
	public void setNombre_departamento(String nombre_departamento) {
		this.nombre_departamento = nombre_departamento;
	}
	public int getId_pais() {
		return id_pais;
	}
	public void setId_pais(int id_pais) {
		this.id_pais = id_pais;
	}
	public String getNombre_pais() {
		return nombre_pais;
	}
	public void setNombre_pais(String nombre_pais) {
		this.nombre_pais = nombre_pais;
	}
	public String getTelefono() {
		return telefono;
	}
	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

}
