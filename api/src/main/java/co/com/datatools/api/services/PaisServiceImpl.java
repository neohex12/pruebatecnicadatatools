package co.com.datatools.api.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.datatools.api.models.PaisModel;
import co.com.datatools.api.repositories.PaisRepository;

@Service
public class PaisServiceImpl implements PaisService {

	@Autowired
	private PaisRepository paisRepository;
	
	@Override
	public Iterable<PaisModel> findAll() {
		return paisRepository.findAll();
	}

	@Override
	public Optional<PaisModel> findById(int id) {
		return paisRepository.findById(id);
	}

}
