package co.com.datatools.api.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.datatools.api.models.ConductorCustomModel;
import co.com.datatools.api.models.ConductorModel;
import co.com.datatools.api.repositories.ConductorCustomQueryRepository;
import co.com.datatools.api.services.ConductorService;

@RestController
@RequestMapping("/api/conductores")
public class ConductorController {
	
	@Autowired
	private ConductorService conductorService;

	@Autowired
	private ConductorCustomQueryRepository conductorCustomQueryRepository;
	
	//crear conductor
	@PostMapping
	public ResponseEntity<?> create (@RequestBody ConductorModel conductorModel){

		ResponseEntity<ConductorModel> control = ResponseEntity.status(HttpStatus.CREATED).body(conductorService.save(conductorModel));

		Map<String, Object> response = new HashMap<>();

		if(control.getStatusCodeValue() != 201) {
			response.put("ok", false);
			response.put("mensaje", "Error al realizar el registro del conductor, si el error persiste comunicarse con el administrador");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		response.put("ok", true);
		response.put("mensaje", "El conductor fue registrado de forma exitosa.");
		response.put("response", control);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}


	//obtener conductor por id
	@GetMapping("/{id}")
	public ResponseEntity<?> obtenerxId (@PathVariable(value = "id") int conductorId){
		Optional<ConductorCustomModel> oConductor = conductorCustomQueryRepository.listarConductorPorId(conductorId);
		Map<String, Object> response = new HashMap<>();
		if(!oConductor.isPresent()) {

			response.put("ok", false);
			response.put("mensaje", "No existe el conductor registrado con el nro Documento: " + conductorId);
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "se encontro una coincidencia del conductor identificado con el nro Documento: " + conductorId);
		response.put("response", oConductor);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}


	//obtener todos los conductores registrados
	@GetMapping("/listarConductores")
	public ResponseEntity<?> listarConductores(){
		List<ConductorCustomModel> lConductor = StreamSupport.stream(conductorCustomQueryRepository.listarConductores().spliterator(),false)
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		if(lConductor.size()==0) {
			response.put("ok", false);
			response.put("mensaje", "No existen conductores registrados en la base de datos");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "Se encontraron "+ lConductor.size()+ " conductores en la consulta");
		response.put("response", lConductor);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}


	//actualizar conductor
	@PutMapping("/{id}")
	public ResponseEntity<?> actualizarConductor (@RequestBody ConductorModel conductorModel, @PathVariable(value = "id") int conductorId){
		Optional<ConductorModel> oConductor = conductorService.findById(conductorId);
		Map<String, Object> response = new HashMap<>();
		if(!oConductor.isPresent()) {
			response.put("ok", false);
			response.put("mensaje", "No es posible encontrar el recurso que desea actualizar");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		oConductor.get().setNombre_completo(conductorModel.getNombre_completo());
		oConductor.get().setDireccion(conductorModel.getDireccion());
		oConductor.get().setId_ciudad(conductorModel.getId_ciudad());
		oConductor.get().setId_departamento(conductorModel.getId_departamento());
		oConductor.get().setId_pais(conductorModel.getId_pais());
		oConductor.get().setTelefono(conductorModel.getTelefono());

		ResponseEntity<ConductorModel> control = ResponseEntity.status(HttpStatus.CREATED).body(conductorService.save(oConductor.get()));

		if(control.getStatusCodeValue() != 201) {
			response.put("ok", false);
			response.put("mensaje", "Error al realizar la actualizacion del conductor, si el error persiste comunicarse con el administrador");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		response.put("ok", true);
		response.put("mensaje", "se actualizo el conductor con nro documento: "+conductorId);
		response.put("response", control);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);



	}

	//eliminar conductor por id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarConductor (@PathVariable(value = "id") int conductorId){
		Optional<ConductorCustomModel> oConductor = conductorCustomQueryRepository.listarConductorPorId(conductorId);
		Map<String, Object> response = new HashMap<>();
		if(!oConductor.isPresent()) {
			response.put("ok", false);
			response.put("mensaje", "No se puede encontrar el conductor a eliminar");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		conductorService.deleteById(conductorId);
		response.put("ok", true);
		response.put("response", "Conductor Eliminado de forma satisfactoria con la identificacion "+conductorId);
		response.put("response", null);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}

}
