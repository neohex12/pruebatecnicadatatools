package co.com.datatools.api.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.datatools.api.models.VehiculoModel;
import co.com.datatools.api.services.VehiculoService;

@RestController
@RequestMapping("/api/vehiculos")
public class VehiculoController  {
	
	@Autowired
	private VehiculoService vehiculoService;
	
	//crear vehiculo
	@PostMapping
	public ResponseEntity<?> create (@RequestBody VehiculoModel vehiculoModel){

		ResponseEntity<VehiculoModel> control = ResponseEntity.status(HttpStatus.CREATED).body(vehiculoService.save(vehiculoModel));

		Map<String, Object> response = new HashMap<>();

		if(control.getStatusCodeValue() != 201) {
			response.put("ok", false);
			response.put("response", "Error al realizar el registro del vehiculo, si el error persiste comunicarse con el administrador");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		response.put("ok", true);
		response.put("mensaje", "El vehiculo fue registrado de forma exitosa.");
		response.put("response", control);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}

	//obtener vehiculo por id
	@GetMapping("/{id}")
	public ResponseEntity<?> obtenerxId (@PathVariable(value = "id") String vehiculo){
		Optional<VehiculoModel> oVehiculo = vehiculoService.findById(vehiculo);
		Map<String, Object> response = new HashMap<>();
		if(!oVehiculo.isPresent()) {

			response.put("ok", false);
			response.put("mensaje", "No existe el vehiculo registrado con la placa: " + vehiculo);
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "se encontro una coincidencia del vehciulo identificado con la placa: " + vehiculo);
		response.put("response", oVehiculo);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}	
	
	
	//obtener todos los vehiculos 
	@GetMapping("/listarVehiculos")
	public ResponseEntity<?> listarVehiculos(){
		List<VehiculoModel> lVehiculo = StreamSupport.stream(vehiculoService.findAll().spliterator(),false)
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		if(lVehiculo.size()==0) {
			response.put("ok", false);
			response.put("mensaje", "No existen vehiculos registrados en la base de datos");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "Se encontraron "+ lVehiculo.size()+ " vehiculos en la consulta");
		response.put("response", lVehiculo);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}
	
	//actualizar vehiculo
	@PutMapping("/{id}")
	public ResponseEntity<?> actualizaVehiculo (@RequestBody VehiculoModel vehiculoModel, @PathVariable(value = "id") String vehiculoId){
		Optional<VehiculoModel> oVehiculo = vehiculoService.findById(vehiculoId);
		Map<String, Object> response = new HashMap<>();
		if(!oVehiculo.isPresent()) {
			response.put("ok", false);
			response.put("mensaje", "No es posible encontrar el recurso que desea actualizar");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		oVehiculo.get().setMotor(vehiculoModel.getMotor());
		oVehiculo.get().setChasis(vehiculoModel.getChasis());
		oVehiculo.get().setModelo(vehiculoModel.getModelo());
		oVehiculo.get().setFecha_matricula(vehiculoModel.getFecha_matricula());
		oVehiculo.get().setPasajeros_sentados(vehiculoModel.getPasajeros_sentados());
		oVehiculo.get().setPasajeros_de_pie(vehiculoModel.getPasajeros_de_pie());
		oVehiculo.get().setPeso_seco(vehiculoModel.getPeso_seco());
		oVehiculo.get().setPeso_bruto(vehiculoModel.getPeso_bruto());
		oVehiculo.get().setCantidad_puertas(vehiculoModel.getCantidad_puertas());
		oVehiculo.get().setMarca(vehiculoModel.getMarca());
		oVehiculo.get().setLinea(vehiculoModel.getLinea());

		ResponseEntity<VehiculoModel> control = ResponseEntity.status(HttpStatus.CREATED).body(vehiculoService.save(oVehiculo.get()));

		if(control.getStatusCodeValue() != 201) {
			response.put("ok", false);
			response.put("mensaje", "Error al realizar la actualizacion del vehiculo, si el error persiste comunicarse con el administrador");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}

		response.put("ok", true);
		response.put("mensaje", "se actualizo el vehiculo con nro documento: "+vehiculoId);
		response.put("response", control);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);



	}
	
	
	//eliminar vehiculo por id
	@DeleteMapping("/{id}")
	public ResponseEntity<?> eliminarVehiculo (@PathVariable(value = "id") String vehiculoId){
		Optional<VehiculoModel> oVehiculo = vehiculoService.findById(vehiculoId);
		Map<String, Object> response = new HashMap<>();
		if(!oVehiculo.isPresent()) {
			response.put("ok", false);
			response.put("mensaje", "No se puede encontrar el vehiculo a eliminar");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		vehiculoService.deleteById(vehiculoId);
		response.put("ok", true);
		response.put("mensaje", "El vehiculo fue eliminado de forma satisfactoria con la placa "+vehiculoId);
		response.put("response", null);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}
}
