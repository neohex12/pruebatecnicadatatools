package co.com.datatools.api.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.datatools.api.models.VehiculoModel;
import co.com.datatools.api.repositories.VehiculoRepository;

@Service
public class VehiculoServiceImpl implements VehiculoService{
	
	@Autowired
	private VehiculoRepository vehiculoRepository;

	@Override
	public Iterable<VehiculoModel> findAll() {
		return vehiculoRepository.findAll();
	}

	@Override
	public Optional<VehiculoModel> findById(String id) {
		return vehiculoRepository.findById(id);
	}

	@Override
	public VehiculoModel save(VehiculoModel vehiculoModel) {
		return vehiculoRepository.save(vehiculoModel);
	}

	@Override
	public void deleteById(String id) {
		vehiculoRepository.deleteById(id);
	}

}
