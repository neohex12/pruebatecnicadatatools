package co.com.datatools.api.services;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import co.com.datatools.api.models.VinculacionModel;
import co.com.datatools.api.repositories.VinculacionRepository;

@Service
public class VinculacionServiceImpl implements VinculacionService {

	@Autowired	
	private VinculacionRepository vinculacionRepository;

	@Override
	public Optional<VinculacionModel> findById(int id) {
		return vinculacionRepository.findById(id);
	}

	@Transactional
	public VinculacionModel save(VinculacionModel empresaModel) {
		return vinculacionRepository.save(empresaModel);
	}

	@Transactional
	public void deleteById(int id) {
		vinculacionRepository.deleteById(id);
	}
	
	

}
