package co.com.datatools.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import co.com.datatools.api.models.VehiculoModel;

@Repository
public interface VehiculoRepository extends JpaRepository<VehiculoModel, String>{

}
