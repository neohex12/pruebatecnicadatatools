package co.com.datatools.api.services;

import java.util.Optional;

import co.com.datatools.api.models.VehiculoModel;


public interface VehiculoService {

	public Iterable<VehiculoModel> findAll();
	public Optional<VehiculoModel> findById(String id);
	public VehiculoModel save(VehiculoModel vehiculoModel);
	public void deleteById(String id);
}
