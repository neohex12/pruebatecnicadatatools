package co.com.datatools.api.models;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Id;

@Entity
@Table(name = "vehiculos")
public class VehiculoModel {
	
	@Id
	private String placa;
	private String motor;
	private String chasis;
	private int modelo;
	private String fecha_matricula;
	private int pasajeros_sentados;
	private int pasajeros_de_pie;
	private String peso_seco;
	private String peso_bruto;
	private int cantidad_puertas;
	private String marca;
	private String linea;
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getMotor() {
		return motor;
	}
	public void setMotor(String motor) {
		this.motor = motor;
	}
	public String getChasis() {
		return chasis;
	}
	public void setChasis(String chasis) {
		this.chasis = chasis;
	}
	public int getModelo() {
		return modelo;
	}
	public void setModelo(int modelo) {
		this.modelo = modelo;
	}
	public String getFecha_matricula() {
		return fecha_matricula;
	}
	public void setFecha_matricula(String fecha_matricula) {
		this.fecha_matricula = fecha_matricula;
	}
	public int getPasajeros_sentados() {
		return pasajeros_sentados;
	}
	public void setPasajeros_sentados(int pasajeros_sentados) {
		this.pasajeros_sentados = pasajeros_sentados;
	}
	public int getPasajeros_de_pie() {
		return pasajeros_de_pie;
	}
	public void setPasajeros_de_pie(int pasajeros_de_pie) {
		this.pasajeros_de_pie = pasajeros_de_pie;
	}
	public String getPeso_seco() {
		return peso_seco;
	}
	public void setPeso_seco(String peso_seco) {
		this.peso_seco = peso_seco;
	}
	public String getPeso_bruto() {
		return peso_bruto;
	}
	public void setPeso_bruto(String peso_bruto) {
		this.peso_bruto = peso_bruto;
	}
	public int getCantidad_puertas() {
		return cantidad_puertas;
	}
	public void setCantidad_puertas(int cantidad_puertas) {
		this.cantidad_puertas = cantidad_puertas;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getLinea() {
		return linea;
	}
	public void setLinea(String linea) {
		this.linea = linea;
	}
	
	
	
}
