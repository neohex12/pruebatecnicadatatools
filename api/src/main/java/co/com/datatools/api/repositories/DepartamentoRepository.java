package co.com.datatools.api.repositories;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import co.com.datatools.api.models.DepartamentoModel;


@Repository
public interface DepartamentoRepository extends JpaRepository<DepartamentoModel, Integer>  {
	
	@Query(value="SELECT id_departamento,id_pais,nombre_departamento FROM `departamentos`", nativeQuery=true)
    List<DepartamentoModel> listarDepartamentos();
	
	@Query(value="SELECT id_departamento,id_pais,nombre_departamento FROM `departamentos` WHERE id_pais = ?", nativeQuery=true)
	List<DepartamentoModel> listarDepartamentosPorId(int id);

}
