package co.com.datatools.api.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import co.com.datatools.api.models.DepartamentoModel;
import co.com.datatools.api.repositories.DepartamentoRepository;

@RestController
@RequestMapping("/api/departamentos")
public class DepartamentoController {
	
	@Autowired
	private DepartamentoRepository departamentoRepository;
	
	//obtener departamento por id
	@GetMapping("/{id}")
	public ResponseEntity<?> obtenerxId (@PathVariable(value = "id") int paisId){
		List<DepartamentoModel> lDepartamento = departamentoRepository.listarDepartamentosPorId(paisId);
		Map<String, Object> response = new HashMap<>();
		if(lDepartamento.size()==0) {
			response.put("ok", false);
			response.put("mensaje", "No existen departamentos registrados asociados con la id pais "+paisId);
			response.put("response", null );
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "Consulta Realizada con exito con el id pais "+paisId);
		response.put("response", lDepartamento);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);

	}	
	
	//obtener todos los departamentos 
	@GetMapping("/listarDepartamentos")
	public ResponseEntity<?> listarDepartamentos(){
		List<DepartamentoModel> lDepartamento = StreamSupport.stream(departamentoRepository.listarDepartamentos().spliterator(),false)
				.collect(Collectors.toList());
		Map<String, Object> response = new HashMap<>();
		if(lDepartamento.size()==0) {
			response.put("ok", false);
			response.put("mensaje", "No existen departamentos registrados en la base de datos");
			response.put("response", null);
			return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
		}
		response.put("ok", true);
		response.put("mensaje", "Se encontraron "+ lDepartamento.size()+ " departamentos en la consulta");
		response.put("response", lDepartamento);
		return new ResponseEntity<>(response, HttpStatus.ACCEPTED);
	}

}
