package co.com.datatools.api.services;

import java.util.Optional;

import co.com.datatools.api.models.ConductorModel;


public interface ConductorService {
	public Optional<ConductorModel> findById(int id);
	public ConductorModel save(ConductorModel conductorModel);
	public void deleteById(int id);
}
