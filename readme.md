##Desarrollo Prueba Técnica para el ingreso a Datatools
Para la prueba se debían realizar dos puntos importantes, la primera parte diseñar la base de datos, realizar un modelo relacional qe permita entender su estructura, luego de eso se debía crear una consulta en base a una condiciones establecidas en el documento de la prueba técnica.

Luego se debía codificar la propuesta, mediante el uso de la base de datos creada, generar un API Rest para la comunicación y por ultimo un cliente para consumir y visualizar la información de los servicios, tanto la API como el Front End fueron desarrollador en Java, utilizando Spring Boot, JPA, Jquery y cmo entorno de desarrollo Uso una versión Personalizada de Eclipse, el cual ya viene adaptada desde la misma página del proveedor. 

##Pre-requisitos 📋
Necesitas los siguientes requisitos para poder hacer funcionar el proyecto:

- Debes tener Java Instalado en la maquina y configuradas las variables de entorno, en una version minima de 1.8.
- Debes tener instalado el motor de base de datos de MySQL, se recomienda hacer uso de herramientas como XAMP o WAMP para ahorrar tiempo en la configuración del mismo.
- Cuando se tengan acceso al MySQL, se debe subir el Scripts de bd_transito.sql para tener creada la estructura de la base  de datos del proyecto  y para poder iniciar con los procesos de la API.
- Primero se debe ejecutar el Api, para poder tener acceso al proyecto, la API Corre bajo el puerto por defecto http://localhost:8080/api
- Luego se debe ejecutar el Front End, Decidi cambiar el puerto porque al usar el por defecto localhost:8080 generaba conflicto de que el puerto ya se encontraba en Uso, por lo cual quedaría asi http://localhost:8090/

Para efecto de pruebas y dudas de ejecucción de Spring Boot  revisar el siguiente enlace:
https://es.stackoverflow.com/questions/374827/ejecutar-proyecto-java-con-spring-en-cmd

##Construido con 🛠️
El Proyecto se construyó utilizando las siguientes herramientas y lenguajes de programación

Jquery- El framework de  Javascript
Maven - Manejador de dependencias.
Spring Boot - Framework de Java.
Java SDK 1.8
Eclipse IDE
MySQL
Boostrap - Framwork de CSS
Html
PostMan

##Versionado 📌
Primera versión del aplicativo, realizado con fines de desmostrar conocimientos técnico - Gitlab


