package co.com.datatools.webapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseCiudad {
   
	@JsonProperty("id_ciudad")
	private int id_ciudad;
	@JsonProperty("id_departamento")
    private int id_departamento;
	@JsonProperty("nombre_ciudad")
    private String nombre_ciudad;
	
	public ResponseCiudad() {
	}

	public ResponseCiudad(int id_ciudad, int id_departamento, String nombre_ciudad) {
		this.id_ciudad = id_ciudad;
		this.id_departamento = id_departamento;
		this.nombre_ciudad = nombre_ciudad;
	}

	public int getId_ciudad() {
		return id_ciudad;
	}

	public void setId_ciudad(int id_ciudad) {
		this.id_ciudad = id_ciudad;
	}

	public int getId_departamento() {
		return id_departamento;
	}

	public void setId_departamento(int id_departamento) {
		this.id_departamento = id_departamento;
	}

	public String getNombre_ciudad() {
		return nombre_ciudad;
	}

	public void setNombre_ciudad(String nombre_ciudad) {
		this.nombre_ciudad = nombre_ciudad;
	}
	
	
	
	
    
}
