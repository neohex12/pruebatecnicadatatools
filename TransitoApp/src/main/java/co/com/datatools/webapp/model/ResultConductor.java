package co.com.datatools.webapp.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResultConductor {
       
	@JsonProperty("response")
    private List<ResponseConductor> response;
	@JsonProperty("mensaje")
	private String mensaje;
	@JsonProperty("ok")
    private String ok;
	
	
	public ResultConductor() {
	}


	public ResultConductor(List<ResponseConductor> response, String mensaje, String ok) {
		this.response = response;
		this.mensaje = mensaje;
		this.ok = ok;
	}


	public List<ResponseConductor> getResponse() {
		return response;
	}


	public void setResponse(List<ResponseConductor> response) {
		this.response = response;
	}


	public String getMensaje() {
		return mensaje;
	}


	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}


	public String getOk() {
		return ok;
	}


	public void setOk(String ok) {
		this.ok = ok;
	}
	
	
	
	
}
