package co.com.datatools.webapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseDepartamento {
	
	@JsonProperty("id_departamento")
	private int id_departamento;
	@JsonProperty("id_pais")
    private int id_pais;
	@JsonProperty("nombre_departamento")
    private String nombre_departamento;
    
	public ResponseDepartamento(int id_departamento, int id_pais, String nombre_departamento) {
		this.id_departamento = id_departamento;
		this.id_pais = id_pais;
		this.nombre_departamento = nombre_departamento;
	}
    
    
	public ResponseDepartamento() {
		
	}


	public int getId_departamento() {
		return id_departamento;
	}


	public void setId_departamento(int id_departamento) {
		this.id_departamento = id_departamento;
	}


	public int getId_pais() {
		return id_pais;
	}


	public void setId_pais(int id_pais) {
		this.id_pais = id_pais;
	}


	public String getNombre_departamento() {
		return nombre_departamento;
	}


	public void setNombre_departamento(String nombre_departamento) {
		this.nombre_departamento = nombre_departamento;
	}
	
	
    	
}
