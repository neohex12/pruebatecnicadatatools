package co.com.datatools.webapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseVehiculo {
	
	@JsonProperty("placa")
	private String placa;
	@JsonProperty("motor")
	private String motor;
	@JsonProperty("chasis")
	private String chasis;
	@JsonProperty("modelo")
	private int modelo;
	@JsonProperty("fecha_matricula")
	private String fecha_matricula;
	@JsonProperty("pasajeros_sentados")
	private int pasajeros_sentados;
	@JsonProperty("pasajeros_de_pie")
	private int pasajeros_de_pie;
	@JsonProperty("peso_seco")
	private String peso_seco;
	@JsonProperty("peso_bruto")
	private String peso_bruto;
	@JsonProperty("cantidad_puertas")
	private int cantidad_puertas;
	@JsonProperty("marca")
	private String marca;
	@JsonProperty("linea")
	private String linea;
	
	public ResponseVehiculo() {
	}

	public ResponseVehiculo(String placa, String motor, String chasis, int modelo, String fecha_matricula,
			int pasajeros_sentados, int pasajeros_de_pie, String peso_seco, String peso_bruto, int cantidad_puertas,
			String marca, String linea) {
		this.placa = placa;
		this.motor = motor;
		this.chasis = chasis;
		this.modelo = modelo;
		this.fecha_matricula = fecha_matricula;
		this.pasajeros_sentados = pasajeros_sentados;
		this.pasajeros_de_pie = pasajeros_de_pie;
		this.peso_seco = peso_seco;
		this.peso_bruto = peso_bruto;
		this.cantidad_puertas = cantidad_puertas;
		this.marca = marca;
		this.linea = linea;
	}

	public String getPlaca() {
		return placa;
	}

	public void setPlaca(String placa) {
		this.placa = placa;
	}

	public String getMotor() {
		return motor;
	}

	public void setMotor(String motor) {
		this.motor = motor;
	}

	public String getChasis() {
		return chasis;
	}

	public void setChasis(String chasis) {
		this.chasis = chasis;
	}

	public int getModelo() {
		return modelo;
	}

	public void setModelo(int modelo) {
		this.modelo = modelo;
	}

	public String getFecha_matricula() {
		return fecha_matricula;
	}

	public void setFecha_matricula(String fecha_matricula) {
		this.fecha_matricula = fecha_matricula;
	}

	public int getPasajeros_sentados() {
		return pasajeros_sentados;
	}

	public void setPasajeros_sentados(int pasajeros_sentados) {
		this.pasajeros_sentados = pasajeros_sentados;
	}

	public int getPasajeros_de_pie() {
		return pasajeros_de_pie;
	}

	public void setPasajeros_de_pie(int pasajeros_de_pie) {
		this.pasajeros_de_pie = pasajeros_de_pie;
	}

	public String getPeso_seco() {
		return peso_seco;
	}

	public void setPeso_seco(String peso_seco) {
		this.peso_seco = peso_seco;
	}

	public String getPeso_bruto() {
		return peso_bruto;
	}

	public void setPeso_bruto(String peso_bruto) {
		this.peso_bruto = peso_bruto;
	}

	public int getCantidad_puertas() {
		return cantidad_puertas;
	}

	public void setCantidad_puertas(int cantidad_puertas) {
		this.cantidad_puertas = cantidad_puertas;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getLinea() {
		return linea;
	}

	public void setLinea(String linea) {
		this.linea = linea;
	}
	
	

}
