package co.com.datatools.webapp.model;

import java.math.BigInteger;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseVinculacion {
	
	@JsonProperty("id")
	private int id;
	@JsonProperty("conductor_asignado")
	private String conductor_asignado;
	@JsonProperty("placa")
	private String placa;
	@JsonProperty("numero_Documento_empresa")
	private BigInteger numero_Documento_empresa;
	@JsonProperty("tipo_Id_Empresa")
	private String tipo_Id_Empresa;
	@JsonProperty("nombre_Empresa")
	private String nombre_Empresa;
	public ResponseVinculacion() {
	}
	public ResponseVinculacion(int id, String conductor_asignado, String placa, BigInteger numero_Documento_empresa,
			String tipo_Id_Empresa, String nombre_Empresa) {
		this.id = id;
		this.conductor_asignado = conductor_asignado;
		this.placa = placa;
		this.numero_Documento_empresa = numero_Documento_empresa;
		this.tipo_Id_Empresa = tipo_Id_Empresa;
		this.nombre_Empresa = nombre_Empresa;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getConductor_asignado() {
		return conductor_asignado;
	}
	public void setConductor_asignado(String conductor_asignado) {
		this.conductor_asignado = conductor_asignado;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public BigInteger getNumero_Documento_empresa() {
		return numero_Documento_empresa;
	}
	public void setNumero_Documento_empresa(BigInteger numero_Documento_empresa) {
		this.numero_Documento_empresa = numero_Documento_empresa;
	}
	public String getTipo_Id_Empresa() {
		return tipo_Id_Empresa;
	}
	public void setTipo_Id_Empresa(String tipo_Id_Empresa) {
		this.tipo_Id_Empresa = tipo_Id_Empresa;
	}
	public String getNombre_Empresa() {
		return nombre_Empresa;
	}
	public void setNombre_Empresa(String nombre_Empresa) {
		this.nombre_Empresa = nombre_Empresa;
	}
	
	
	
	
	

}
