package co.com.datatools.webapp.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;




public class ResultEmpresa {
	@JsonProperty("response")
    private List<ResponseEmpresa> response;
	@JsonProperty("mensaje")
	private String mensaje;
	@JsonProperty("ok")
    private String ok;
    
	public ResultEmpresa(List<ResponseEmpresa> response, String mensaje, String ok) {
		this.response = response;
		this.mensaje = mensaje;
		this.ok = ok;
	}

	public ResultEmpresa() {
		
	}

	public List<ResponseEmpresa> getResponse() {
		return response;
	}

	public void setResponse(List<ResponseEmpresa> response) {
		this.response = response;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}

	
	 
}
