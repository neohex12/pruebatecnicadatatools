package co.com.datatools.webapp.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResultRepresentanteLegal {
	
	@JsonProperty("response")
    private List<ResponseRepresentanteLegal> response;
	@JsonProperty("mensaje")
	private String mensaje;
	@JsonProperty("ok")
    private String ok;
	
	public ResultRepresentanteLegal() {
		
	}

	public ResultRepresentanteLegal(List<ResponseRepresentanteLegal> response, String mensaje, String ok) {
		this.response = response;
		this.mensaje = mensaje;
		this.ok = ok;
	}

	public List<ResponseRepresentanteLegal> getResponse() {
		return response;
	}

	public void setResponse(List<ResponseRepresentanteLegal> response) {
		this.response = response;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public String getOk() {
		return ok;
	}

	public void setOk(String ok) {
		this.ok = ok;
	}

    

}
