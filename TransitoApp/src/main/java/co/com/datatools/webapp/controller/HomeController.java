package co.com.datatools.webapp.controller;


import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.util.List;



import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;



import co.com.datatools.webapp.model.ResponseCiudad;
import co.com.datatools.webapp.model.ResponseDepartamento;
import co.com.datatools.webapp.model.ResultCiudad;
import co.com.datatools.webapp.model.ResultConductor;
import co.com.datatools.webapp.model.ResultDepartamento;
import co.com.datatools.webapp.model.ResultEmpresa;
import co.com.datatools.webapp.model.ResultPais;
import co.com.datatools.webapp.model.ResultRepresentanteLegal;
import co.com.datatools.webapp.model.ResultVehiculo;
import co.com.datatools.webapp.model.ResultVinculacion;

import org.springframework.ui.Model;

@Controller
public class HomeController {
	
	final String urlApi = "http://localhost:8080/api";

	@GetMapping("/")
	public String index() {
		return "index";
	}

	@GetMapping("/gestionarEmpresas")
	public String gestionarEmpresas(Model model) {
		return "gestionarEmpresas";
	}

	@GetMapping("/gestionarRepresentantes")
	public String gestionarRepresentantes(Model model) {
		return "gestionarRepresentantes";
	}
	
	@GetMapping("/gestionarVehiculos")
	public String gestionarVehiculos(Model model) {
		return "gestionarVehiculos";
	}
	
	@GetMapping("/gestionarConductores")
	public String gestionarConductores(Model model) {
		return "gestionarConductores";
	}
	
	@GetMapping("/gestionarVinculaciones")
	public String gestionarVinculaciones(Model model) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ResultEmpresa> responseEntity = restTemplate.getForEntity(urlApi+"/empresas/listarEmpresas", ResultEmpresa.class); 
		ResultEmpresa result = responseEntity.getBody();
		model.addAttribute("empresas", result.getResponse());
		return "gestionarVinculaciones";
	}

	@GetMapping("/listarEmpresas")
	public String listarEmpresas(Model model) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ResultEmpresa> responseEntity = restTemplate.getForEntity(urlApi+"/empresas/listarEmpresas", ResultEmpresa.class); 
		ResultEmpresa result = responseEntity.getBody();
		model.addAttribute("empresas", result.getResponse());
		return "listarEmpresas";
	}

	@GetMapping("/listarRepresentantes")
	public String listarRepresentantes(Model model) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ResultRepresentanteLegal> responseEntity = restTemplate.getForEntity(urlApi+"/representante/listarRepresentantes", ResultRepresentanteLegal.class); 
		ResultRepresentanteLegal result = responseEntity.getBody();
		model.addAttribute("representantes", result.getResponse());
		return "listarRepresentantes";
	}
	
	@GetMapping("/listarVehiculos")
	public String listarVehiculos(Model model) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ResultVehiculo> responseEntity = restTemplate.getForEntity(urlApi+"/vehiculos/listarVehiculos", ResultVehiculo.class); 
		ResultVehiculo result = responseEntity.getBody();
		model.addAttribute("vehiculos", result.getResponse());
		return "listarVehiculos";
	}
	
	@GetMapping("/listarConductores")
	public String listarConductores(Model model) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ResultConductor> responseEntity = restTemplate.getForEntity(urlApi+"/conductores/listarConductores", ResultConductor.class); 
		ResultConductor result = responseEntity.getBody();
		model.addAttribute("conductores", result.getResponse());
		return "listarConductores";
	}
	
	@GetMapping("/listarVinculaciones/{id}")
	public String listarVinculacionesPorEmpresa(@PathVariable(value = "id") String empresaId,Model model) {
		System.out.println(empresaId);
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ResultVinculacion> responseEntity = restTemplate.getForEntity(urlApi+"/vinculaciones/listarVinculaciones/"+empresaId, ResultVinculacion.class); 
		ResultVinculacion result = responseEntity.getBody();
		model.addAttribute("vinculaciones", result.getResponse());
		return "listarVinculaciones";
	}
	
	@ResponseBody
	@DeleteMapping("/desvincularVYC/{id}")
	public void desvincularVYC(@PathVariable(value = "id") String vinculacionId) {
		//System.out.println(empresaId);
		RestTemplate restTemplate = new RestTemplate();
		restTemplate.delete(urlApi+"/vinculaciones/"+vinculacionId); 
	}

	@GetMapping("/modalCamposEmpresa")
	public String cargarComponenteModalEmpresa(Model model){	
		ResultPais resultPais = ObtenerPaises().getBody();
		ResultRepresentanteLegal resultRepresentante = ObtenerRepresentantesLegales().getBody();
		model.addAttribute("paises", resultPais.getResponse());
		model.addAttribute("representantes", resultRepresentante.getResponse());
		return "modalAgregarEmpresa";
	}

	@GetMapping("/modalCamposRepresentante")
	public String cargarComponenteModalRepresentante(Model model){	
		ResultPais resultPais = ObtenerPaises().getBody();
		model.addAttribute("paises", resultPais.getResponse());
		return "modalAgregarRepresentante";
	}
	
	@GetMapping("/modalCamposVinculacion")
	public String cargarComponenteModalVinculacion(Model model){	
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<ResultConductor> responseEntity = restTemplate.getForEntity(urlApi+"/conductores/listarConductores", ResultConductor.class); 
		ResultConductor result = responseEntity.getBody();
		model.addAttribute("conductores", result.getResponse());
		ResponseEntity<ResultVehiculo> responseEntityV = restTemplate.getForEntity(urlApi+"/vehiculos/listarVehiculos", ResultVehiculo.class); 
		ResultVehiculo resultV = responseEntityV.getBody();
		model.addAttribute("vehiculos", resultV.getResponse());
		ResponseEntity<ResultEmpresa> responseEntityE = restTemplate.getForEntity(urlApi+"/empresas/listarEmpresas", ResultEmpresa.class); 
		ResultEmpresa resultE = responseEntityE.getBody();
		model.addAttribute("empresas", resultE.getResponse());
		return "modalAgregarVinculacion";
	}
	
	@GetMapping("/modalCamposVehiculo")
	public String cargarComponenteModalVehiculo(Model model){	
		return "modalAgregarVehiculo";
	}
	
	@GetMapping("/modalCamposConductor")
	public String cargarComponenteModalConductor(Model model){	
		ResultPais resultPais = ObtenerPaises().getBody();
		model.addAttribute("paises", resultPais.getResponse());
		return "modalAgregarConductor";
	}


	@ResponseBody
	@GetMapping("/obtenerDepartamentoPorId/{id}")
	public List<ResponseDepartamento> obtenerDepartamentoporId(@PathVariable(value = "id") int paisId){	
		ResultDepartamento resultDepartamentoxPais = ObtenerDepartamentoPorId(paisId).getBody();
		return resultDepartamentoxPais.getResponse();
	}

	@ResponseBody
	@GetMapping("/obtenerCiudadPorId/{id}")
	public List<ResponseCiudad> obtenerCiudadporId(@PathVariable(value = "id") int departamentoId){	
		ResultCiudad resultCiudadxDepartamento = ObtenerCiudadPorId(departamentoId).getBody();
		return resultCiudadxDepartamento.getResponse();
	}

	public ResponseEntity<ResultPais> ObtenerPaises(){
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForEntity(urlApi+"/paises/listarPaises", ResultPais.class);
	}

	public ResponseEntity<ResultRepresentanteLegal> ObtenerRepresentantesLegales() {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForEntity(urlApi+"/representante/listarRepresentantes", ResultRepresentanteLegal.class);
	}

	public ResponseEntity<ResultDepartamento> ObtenerDepartamentoPorId(int id){
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForEntity(urlApi+"/departamentos/"+id, ResultDepartamento.class);
	}

	public ResponseEntity<ResultCiudad> ObtenerCiudadPorId(int id){
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForEntity(urlApi+"/ciudades/"+id, ResultCiudad.class);
	}



	@ResponseBody
	@PostMapping(path = "/guardarEmpresa" , consumes = MediaType.APPLICATION_JSON_VALUE)
	public String createEmpresa (@RequestBody String data) throws JSONException, UnsupportedEncodingException{

		RestTemplate restTemplate = new RestTemplate();
		JSONObject request = ConvertirEmpresa(data);
		JSONObject restJson = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> loginResponse = restTemplate
				.exchange(urlApi+"/empresas", HttpMethod.POST, entity, String.class);
		if(loginResponse.getStatusCode() == HttpStatus.OK) {
			try {
				restJson = new JSONObject(loginResponse.getBody());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return loginResponse.getBody();

	}

	@ResponseBody
	@PostMapping(path = "/guardarRepresentante" , consumes = MediaType.APPLICATION_JSON_VALUE)
	public String createRepresentante (@RequestBody String data) throws JSONException, UnsupportedEncodingException{

		RestTemplate restTemplate = new RestTemplate();
		JSONObject request = ConvertirPersona(data);
		JSONObject restJson = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> loginResponse = restTemplate
				.exchange(urlApi+"/representante", HttpMethod.POST, entity, String.class);
		if(loginResponse.getStatusCode() == HttpStatus.OK) {
			try {
				restJson = new JSONObject(loginResponse.getBody());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return loginResponse.getBody();

	}
	
	@ResponseBody
	@PostMapping(path = "/guardarConductor" , consumes = MediaType.APPLICATION_JSON_VALUE)
	public String createConductor (@RequestBody String data) throws JSONException, UnsupportedEncodingException{

		RestTemplate restTemplate = new RestTemplate();
		JSONObject request = ConvertirPersona(data);
		JSONObject restJson = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> loginResponse = restTemplate
				.exchange(urlApi+"/conductores", HttpMethod.POST, entity, String.class);
		if(loginResponse.getStatusCode() == HttpStatus.OK) {
			try {
				restJson = new JSONObject(loginResponse.getBody());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return loginResponse.getBody();

	}
	
	@ResponseBody
	@PostMapping(path = "/guardarVehiculo" , consumes = MediaType.APPLICATION_JSON_VALUE)
	public String createVehiculo (@RequestBody String data) throws JSONException, UnsupportedEncodingException{

		RestTemplate restTemplate = new RestTemplate();
		JSONObject request = ConvertirVehiculo(data);
		JSONObject restJson = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> loginResponse = restTemplate
				.exchange(urlApi+"/vehiculos", HttpMethod.POST, entity, String.class);
		if(loginResponse.getStatusCode() == HttpStatus.OK) {
			try {
				restJson = new JSONObject(loginResponse.getBody());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return loginResponse.getBody();

	}
	
	
	@ResponseBody
	@PostMapping(path = "/guardarVinculacion" , consumes = MediaType.APPLICATION_JSON_VALUE)
	public String createVinculacion (@RequestBody String data) throws JSONException, UnsupportedEncodingException{

		RestTemplate restTemplate = new RestTemplate();
		JSONObject request = ConvertirVinculo(data);
		JSONObject restJson = null;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(request.toString(), headers);

		ResponseEntity<String> loginResponse = restTemplate
				.exchange(urlApi+"/vinculaciones", HttpMethod.POST, entity, String.class);
		if(loginResponse.getStatusCode() == HttpStatus.OK) {
			try {
				restJson = new JSONObject(loginResponse.getBody());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return loginResponse.getBody();

	}


	public JSONObject ConvertirEmpresa(String data) throws JSONException, UnsupportedEncodingException{
		String decoded = URLDecoder.decode(data, "UTF-8");
		String[] cadena_original = decoded.split("&");
		JSONObject request = new JSONObject();
		request.put("tipo", Integer.parseInt(cadena_original[0].split("=")[1]));
		request.put("nro_documento_empresa",new BigInteger(cadena_original[1].split("=")[1]));
		request.put("documento_representante_legal", Integer.parseInt(cadena_original[2].split("=")[1]));
		request.put("nombre_completo", cadena_original[3].split("=")[1]);
		request.put("direccion", cadena_original[4].split("=")[1]);
		request.put("id_pais", Integer.parseInt(cadena_original[5].split("=")[1]));
		request.put("id_departamento", Integer.parseInt(cadena_original[6].split("=")[1]));
		request.put("id_ciudad", Integer.parseInt(cadena_original[7].split("=")[1]));
		request.put("telefono", cadena_original[8].split("=")[1]);

		return request;
	}

	public JSONObject ConvertirPersona(String data) throws JSONException, UnsupportedEncodingException{
		String decoded = URLDecoder.decode(data, "UTF-8");
		String[] cadena_original = decoded.split("&");
		JSONObject request = new JSONObject();
		request.put("tipo_documento", Integer.parseInt(cadena_original[0].split("=")[1]));
		request.put("nro_documento",Integer.parseInt(cadena_original[1].split("=")[1]));
		request.put("nombre_completo", cadena_original[2].split("=")[1]);
		System.out.println(cadena_original[3].split("=")[1]);
		request.put("direccion", cadena_original[3].split("=")[1]);
		request.put("id_pais", Integer.parseInt(cadena_original[4].split("=")[1]));
		request.put("id_departamento", Integer.parseInt(cadena_original[5].split("=")[1]));
		request.put("id_ciudad", Integer.parseInt(cadena_original[6].split("=")[1]));
		request.put("telefono", cadena_original[7].split("=")[1]);

		return request;
	}
	
	
	public JSONObject ConvertirVehiculo(String data) throws JSONException, UnsupportedEncodingException{
		String decoded = URLDecoder.decode(data, "UTF-8");
		String[] cadena_original = decoded.split("&");
		JSONObject request = new JSONObject();
		request.put("placa", cadena_original[0].split("=")[1]);
		request.put("motor",cadena_original[1].split("=")[1]);
		request.put("chasis", cadena_original[2].split("=")[1]);
		request.put("modelo", Integer.parseInt(cadena_original[3].split("=")[1]));
		request.put("fecha_matricula", cadena_original[4].split("=")[1]);
		request.put("pasajeros_sentados", Integer.parseInt(cadena_original[5].split("=")[1]));
		request.put("pasajeros_de_pie", Integer.parseInt(cadena_original[6].split("=")[1]));
		request.put("peso_seco", cadena_original[7].split("=")[1]);
		request.put("peso_bruto", cadena_original[8].split("=")[1]);
		request.put("cantidad_puertas", Integer.parseInt(cadena_original[9].split("=")[1]));
		request.put("marca", cadena_original[10].split("=")[1]);
		request.put("linea", cadena_original[11].split("=")[1]);

		return request;
	}private BigInteger id_empresa;
	private String placa_vehiculo;
	private BigInteger conductor_asignado;
	
	public JSONObject ConvertirVinculo(String data) throws JSONException, UnsupportedEncodingException{
		String decoded = URLDecoder.decode(data, "UTF-8");
		String[] cadena_original = decoded.split("&");
		System.out.println(decoded);
		JSONObject request = new JSONObject();
		request.put("id_empresa", cadena_original[2].split("=")[1]);
		request.put("placa_vehiculo",cadena_original[1].split("=")[1]);
		request.put("conductor_asignado", cadena_original[0].split("=")[1]);
        

		return request;
	}
	
}
