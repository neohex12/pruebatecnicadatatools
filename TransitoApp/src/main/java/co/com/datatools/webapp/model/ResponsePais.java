package co.com.datatools.webapp.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponsePais {
	
	@JsonProperty("id_pais")
	private int id_pais;
	@JsonProperty("iso")
	private String iso;
	@JsonProperty("nombre_pais")
	private String nombre_pais;
	
	public ResponsePais(int id_pais, String iso, String nombre_pais) {
		this.id_pais = id_pais;
		this.iso = iso;
		this.nombre_pais = nombre_pais;
	}
	
	
	public ResponsePais(){
		
	}


	public int getId_pais() {
		return id_pais;
	}


	public void setId_pais(int id_pais) {
		this.id_pais = id_pais;
	}


	public String getIso() {
		return iso;
	}


	public void setIso(String iso) {
		this.iso = iso;
	}


	public String getNombre_pais() {
		return nombre_pais;
	}


	public void setNombre_pais(String nombre_pais) {
		this.nombre_pais = nombre_pais;
	}
	
	

}
