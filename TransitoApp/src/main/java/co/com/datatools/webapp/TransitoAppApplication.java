package co.com.datatools.webapp;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TransitoAppApplication {

	public static void main(String[] args) {
		/*SpringApplicationapp.setDefaultProperties(Collections
		          .singletonMap("server.port", "8090")).run(TransitoAppApplication.class, args);*/
		
		SpringApplication app = new SpringApplication(TransitoAppApplication.class);
        app.setDefaultProperties(Collections
          .singletonMap("server.port", "8090"));
        app.run(args);
	}

}
